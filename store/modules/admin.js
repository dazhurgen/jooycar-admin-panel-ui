/*jshint esversion: 6 */
// import Vue from 'vue';
 import axios from 'axios';

const state = {
    adminsList:[]
};
const getters = {
    selectedMenu: state => {
        return state.selectedMenu;
    },
    adminsList: stat =>{
        return state.adminsList;
    }
};
const actions = {
    getAdminList(context, payload){
        axios.get("http://localhost:3000/api/admins/v1").then(result =>{
            console.log('result');
            console.log(result);
            context.commit("updateAdminsList", result.data);
        })
    },
    updateAdmin(context, payload){
        return new Promise((resolve, reject)=>{
            axios.patch('http://localhost:3000/api/admins/v1/'+payload.id, payload.data).then(result =>{
                console.log('result');
                console.log(result);
                resolve(result);
            }).catch(error=>{
                reject(error);
            })
        })
       
    },
    addAdmin(context, payload){
        return new Promise((resolve, reject)=>{
            axios.post('http://localhost:3000/api/admins/v1/', payload).then(result =>{
                console.log('result');
                console.log(result);
                resolve(result);
            }).catch(error=>{
                reject(error);
            })
        })
    },
    deleteAdmin(context, payload){
        return new Promise((resolve, reject)=>{
            axios.delete('http://localhost:3000/api/admins/v1/'+payload.id).then(result =>{
                console.log('result');
                console.log(result);
                resolve(result);
            }).catch(error=>{
                reject(error);
            })
        })
    }


};
const mutations = {
    updateAdminsList(state, data){
        state.adminsList = data;
    }
};
export default {
    state,
    getters,
    actions,
    mutations
}